const router = require('express').Router();
const user = require('./controller/userController');
const task = require('./controller/taskController');
const profile = require('./controller/profileController');

const authenticate = require('./middleware/authenticate');
const success = require('./middleware/success');
const checkUser = require('./middleware/checkOwnership');
const upload = require('./middleware/uploader');

router.post('/user/register', user.register);
router.post('/user/login', user.login);
router.put('/user',authenticate, upload.single('image'),profile.updateProfile);
router.get('/user', authenticate, user.userData);

router.post('/tasks', authenticate, task.create, success);
router.put('/tasks/:id', authenticate, checkUser('Task'), task.update, success);
router.delete('/tasks/:id', authenticate, checkUser('Task'), task.delete, success);
router.get('/tasks', authenticate, task.findAll, success);
router.get('/tasks/order', authenticate, task.findAllSorted, success);
router.get('/tasks/completed', authenticate, task.findAllCompleted, success);
router.get('/tasks/important', authenticate, task.findAllImportance, success);

module.exports = router;