'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    picture: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    user_id: DataTypes.INTEGER
  }, {
    tableName : "profiles"
  });
  Profile.associate = function (models) {
    // associations can be defined here
    Profile.belongsTo(models.User, {
      foreignKey: 'user_id',
    })
  };
  return Profile;
};