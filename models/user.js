'use strict';

const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: { 
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: 'please fill by email'
        },
        isLowercase: true
      }
    },
    encrypted_password: DataTypes.STRING,
  },  {
    tableName : "users"
  });
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Task, {
      foreignKey: 'user_id'
    }),
    User.hasOne(models.Profile, {
      foreignKey: 'user_id'
    })
  };
  return User;
};