const models = require('../models');

module.exports = (modelName) => {
  const Model = models[modelName];

  return async function(req, res, next) {
    try {
      console.log({Model})
      let instance = await Model.findByPk(req.params.id)

      console.log(`${instance.user_id} => ${req.params.id}`)

      if (instance.user_id !== req.user.id) {
        res.status(403);
        return next(new Error(`This is not your ${modelName}`))
      }
      next();
    } catch (error) {
      next(error)
    }
  }
}