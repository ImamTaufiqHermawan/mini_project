const { Task } = require('../models')

exports.create = async function (req, res, next) {
    try {
        let task = await Task.create({
            name: req.body.name,
            description: req.body.description,
            due_date: req.body.due_date,
            importance: req.body.importance,
            completed: req.body.completed,
            user_id: req.user.id
        });
        res.status(200);
        res.data = { task };
        next();
    }
    catch (err) {
        console.log({ err })
        res.status(422);
        next(err)
    }
}

exports.update = async function (req, res, next) {
    try {
        let { name, description, due_date, importance, completed } = req.body

        let dataUpdate = {
            name,
            description,
            due_date,
            importance,
            completed
        }

        await Task.update(dataUpdate, {
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully updated";
        next();
    }
    catch (err) {
        res.status(422);
        next(err);
    }
}

exports.delete = async function (req, res, next) {
    try {
        await Task.destroy({
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully deleted";
        next();
    }
    catch (err) {
        res.status(500);
        next(err);
    }
}

exports.findAll = async function (req, res, next) {
    const query = req.query;
    console.log({ query })
    try {
        const tasks = await Task.findAll({
            where: {
                user_id: req.user.id
            },
            limit: query.limit <= 10 ? query.limit : 10,
            offset: query.offset,
            order: [
                ['id', 'ASC']
            ]
        })
        res.status(200);
        res.data = { tasks };
        next();
    }
    catch (err) {
        res.status(404);
        next(err);
    }
}

exports.findAllSorted = async function (req, res, next) {
    const query = req.query;
    console.log({ query })
    try {
        const tasks = await Task.findAll({
            where: {
                user_id: req.user.id
            },
            limit: query.limit <= 10 ? query.limit : 10,
            offset: query.offset,
            order: [
                ['completed', 'DESC'],
                ['importance', 'DESC']
            ]
        })
        res.status(200)
        res.data = { tasks };
        next()
    } catch (err) {
        next(err)
    }
}

exports.findAllCompleted = async function (req, res, next) {
    const query = req.query;
    console.log({ query })
    try {
        const tasks = await Task.findAll({
            where: {
                user_id: req.user.id,
                completed: true
            },
            limit: query.limit <= 10 ? query.limit : 10,
            offset: query.offset,
            order: [
                ['completed', 'ASC'],
                ['importance', 'ASC']
            ]
        })
        res.status(200)
        res.data = { tasks };
        next()
    } catch (err) {
        next(err)
    }
}

exports.findAllImportance = async function (req, res, next) {
    const query = req.query;
    console.log({ query })
    try {
        const tasks = await Task.findAll({
            where: {
                user_id: req.user.id,
                importance: true
            },
            limit: query.limit <= 10 ? query.limit : 10,
            offset: query.offset,
            order: [
                ['completed', 'ASC'],
                ['importance', 'ASC']
            ]
        })
        res.status(200)
        res.data = { tasks };
        next()
    } catch (err) {
        next(err)
    }
}