require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const PORT = process.env.PORT || 3000;

const router = require('./router');
const exception = require('./middleware/exception');

app.use(express.json());
app.use(cors());

if(process.env.NODE_ENV !== 'test') app.use(morgan('dev'))

app.get('/', (req, res) => {
    res.status(200).json({
        "status": "success",
        "message": "HELLO WORLD"
    })
})

app.use('/api/v1/', router)

exception.forEach(handler =>
    app.use(handler)
  )

module.exports=app;